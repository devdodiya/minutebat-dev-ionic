// Project Name: IonicEcommerce
// Project URI: http://ionicecommerce.com
// Author: VectorCoder Team
// Author URI: http://vectorcoder.com/
import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../../providers/config/config';
import { TranslateService } from '@ngx-translate/core';
import { SharedDataProvider } from '../../providers/shared-data/shared-data';
import { trigger, style, animate, transition } from '@angular/animations';
import { ProductsPage } from '../products/products';
import { NavController, Content, Events, ToastController } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { SearchPage } from '../search/search';
import { SubCategoriesPage } from '../sub-categories/sub-categories'; 

import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@Component({
  selector: 'page-home',
  animations: [
    trigger(
      'animate', [
        transition(':enter', [
          style({ opacity: 0 }),
          animate('500ms', style({ opacity: 1 }))
        ]),
        transition(':leave', [
          style({ opacity: 1 }),
          animate('700ms', style({ opacity: 0 }))
        ])
      ]
    )
  ],
  templateUrl: 'home.html',
})

export class HomePage {

  @ViewChild(Content) content: Content;

  scrollToTop() {
    this.content.scrollToTop(700);
    this.scrollTopButton = false;
  }

  onScroll(e) {

    if (e.scrollTop >= 1200) this.scrollTopButton = true;
    if (e.scrollTop < 1200) this.scrollTopButton = false;
    //else this.scrollTopButton=false;
    //   console.log(e);
  }
  scrollTopButton = false;
  segments: any = 'topSeller';
  userCurrentLocation : any ="";
  flag="";
  constructor(
    public http: Http,
    public config: ConfigProvider,
    public shared: SharedDataProvider, 
    public events: Events, 
    translate: TranslateService,
    public navCtrl: NavController, public geolocation: Geolocation, public geocoder: NativeGeocoder, public toaster: ToastController, public locac: LocationAccuracy) {
    this.geolocate();  
    }
  
  
  geolocate() {
    let options = {
      enableHighAccuracy: true
    };
    this.locac.canRequest().then((res: boolean) => {
      
      if(res) {
        // the accuracy option will be ignored by iOS
        this.locac.request(this.locac.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => console.log('Location Service Activated.'),
          error => alert('Error requesting location permissions'+ error)
        );
      }

         this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
         this.geocoder.reverseGeocode(position.coords.latitude, position.coords.longitude).then((res: NativeGeocoderReverseResult[]) => {
         this.userCurrentLocation = res[0].subLocality+", "+res[0].thoroughfare+", "+res[0].locality+", "+res[0].subAdministrativeArea+", "+res[0].administrativeArea;
         //this.navCtrl.setRoot(this.navCtrl.getActive().component);

      }) 
      }).catch((err) => {
        alert(err);
      }) 
    })
    
  }
 
  openSubCategories(parent) {
    let count = 0; 
    for (let value of this.shared.allCategories) {
      console.log()
      if (value.parent == parent) { count++; console.log(value) }
    } 
    if (count != 0){ 
     // alert(parent);
      this.navCtrl.push(SubCategoriesPage, { 'parent': parent });
    }
    else{
      this.navCtrl.push(ProductsPage, { id: parent, name: "" });
    }
  }
  openProducts(value) {
    this.navCtrl.push(ProductsPage, { type: value });
  }
  ngAfterContentChecked() {
    this.content.resize();
  }

  openCart() {
    this.navCtrl.push(CartPage);
  }
  openSearch() {
    this.navCtrl.push(SearchPage);
  }

}
