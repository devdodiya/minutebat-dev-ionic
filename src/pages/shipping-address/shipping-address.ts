// Project Name: IonicEcommerce
// Project URI: http://ionicecommerce.com
// Author: VectorCoder Team
// Author URI: http://vectorcoder.com/
import { Component, ApplicationRef } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';
import { Http } from '@angular/http';
import { LoadingProvider } from '../../providers/loading/loading';
import { SharedDataProvider } from '../../providers/shared-data/shared-data';
import { SelectCountryPage } from '../select-country/select-country';
import { SelectZonesPage } from '../select-zones/select-zones';
import { BillingAddressPage } from '../billing-address/billing-address';
import { LocationDataProvider } from '../../providers/location-data/location-data';


import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { ShippingMethodPage } from '../shipping-method/shipping-method';

@Component({
  selector: 'page-shipping-address',
  templateUrl: 'shipping-address.html',
})
export class ShippingAddressPage {

  userCurrentLocation : any ="";
  userAddressOne : any ="";
  userAddressTwo : any ="";
  userState : any ="";
  userCity : any ="";
  userPostalCode : any ="";
  userCountry : any ="";
  userCompany : any = "";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public config: ConfigProvider,
    public http: Http,
    public shared: SharedDataProvider,
    public modalCtrl: ModalController,
    public loading: LoadingProvider,
    public location: LocationDataProvider,
    public geolocation: Geolocation, 
    public geocoder: NativeGeocoder, 
    public locac: LocationAccuracy,
    public applicationRef:ApplicationRef
    ) {
      if (this.shared.customerData.id != null) {
        this.shared.billing = this.shared.customerData.billing;
        this.shared.billing.email = this.shared.customerData.email;
        this.shared.billing.phone = this.shared.customerData.phone;
        this.shared.billingCountryName = this.location.getCountryName(this.shared.customerData.billing.country);
        this.shared.billingStateName = this.location.getStateName(this.shared.customerData.billing.country, this.shared.customerData.billing.state);
      }
    this.geolocate();  
    //this.getAllTaxRates(1);
  }

  geolocate() {
    let options = {
      enableHighAccuracy: true
    };
    this.locac.canRequest().then((res: boolean) => { 
      this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
          this.geocoder.reverseGeocode(position.coords.latitude, position.coords.longitude).then((res: NativeGeocoderReverseResult[]) => {
          this.userCurrentLocation = res[0].subLocality+", "+res[0].thoroughfare+", "+res[0].locality+", "+res[0].subAdministrativeArea+", "+res[0].administrativeArea;
          this.userAddressOne =  res[0].subLocality;
          this.userAddressTwo = res[0].thoroughfare+", "+res[0].locality ;
          this.userState = res[0].administrativeArea;
          this.userCity = res[0].subAdministrativeArea;
          this.userCountry ="India";
          this.userPostalCode = res[0].postalCode; 
          this.userCompany= "none";
          //alert(res);
      }) 
      }).catch((err) => {
        alert(err);
      }) 
    })
    
  }

  submit() {
    // this.navCtrl.push(BillingAddressPage);
    this.shared.billing.first_name = this.shared.shipping.first_name;
    this.shared.billing.last_name = this.shared.shipping.last_name;
    this.shared.billing.state = this.shared.shipping.state;
    this.shared.billing.postcode = this.shared.shipping.postcode;
    this.shared.billing.country = this.shared.shipping.country;
    this.shared.billing.address_1 = this.shared.shipping.address_1;
    this.shared.billing.address_2 = this.shared.shipping.address_2;
    this.shared.billing.city = this.shared.shipping.city;
    this.shared.billing.company = this.shared.shipping.company;
    this.shared.billingCountryName = this.shared.shippingCountryName;
    this.shared.billingStateName = this.shared.shippingStateName;
    this.shared.billing.phone = this.shared.customerData.phone;
    this.shared.billing.email = this.shared.customerData.email;
    
    this.navCtrl.push(ShippingMethodPage);
    this.applicationRef.tick();
    
  }
  selectCountryPage() {
    let modal = this.modalCtrl.create(SelectCountryPage, { page: 'shipping' });
    modal.present();
  }
  selectZonePage() {
    let modal = this.modalCtrl.create(SelectZonesPage, { page: 'shipping' });
    modal.present();
  }
  getAllTaxRates(page) {
    this.config.Woocommerce.getAsync("taxes/?per_page=100&page=" + page).then((data) => {
      var dat = JSON.parse(data.body);
      for (let val of dat) {
        this.shared.listTaxRates.push(val);
      }
      if (dat.length != 0) { this.getAllTaxRates(page + 1); }
      //else console.log(this.shared.listTaxRates);
    });
  }
}
